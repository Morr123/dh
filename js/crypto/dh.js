﻿var CryptoJSAesJson = {
    stringify: function (cipherParams) {
        var j = {ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64)};
        if (cipherParams.iv) j.iv = cipherParams.iv.toString();
        if (cipherParams.salt) j.s = cipherParams.salt.toString();
        return JSON.stringify(j);
    },
    parse: function (jsonStr) {
        var j = JSON.parse(jsonStr);
        var cipherParams = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Base64.parse(j.ct)});
        if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv)
        if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s)
        return cipherParams;
    }
}

function DH(endTrade){
	
	this.secret_key = null;
	this.a = null;
	this.p = null;
	this.mod = null;
	this.myPriv = null;
	
	this.request = function(url, data = {}, func)
	{
		var self = this;
		$.ajax({
			url: url,
			type: 'POST',
			data: data,
			/*data: {
				dh: {a:this.a.toString(), p:this.p.toString(), mod:this.mod.toString()},
				data: data,
			},*/
			success: function(data)
			{
				func(data);
			}
		});
	}
	
	this.generateSecretKey = function()
	{
		var self = this;
		this.request('secret.php', {dh: {a:this.a.toString(), p:this.p.toString(), mod:this.mod.toString()}}, function(data){
			data = JSON.parse(data);
			var mod = new bigInt(data.mod);
			var secret = fastpow(mod, self.myPriv, self.p);
			self.secret_key = secret.toString();
				
			$('#test2').html(data.mod);
			$('#secret2').html(data.secret);
			$('#secret1').html(secret.toString());
			
			endTrade();
		});
	}
	
	
	this.send = function(url, data, func){
		var self = this;
		var hash = CryptoJS.AES.encrypt(JSON.stringify(data), this.secret_key, {format: CryptoJSAesJson}).toString();
		this.request('secret.php', {data: hash}, function(data){
			var decrypted = JSON.parse(CryptoJS.AES.decrypt(data, self.secret_key, {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8));
			func(decrypted);
		});	
	}
	
	/***********CUNSTRUCT*********/
	var rng = new SecureRandom();
	var sch = 0;
	this.a = pick_rand();
	this.p = pick_rand();
	this.myPriv = pick_rand(); 
	this.mod = fastpow(this.a, this.myPriv, this.p);
	this.generateSecretKey();
	
	$('#priv1').html(this.a.toString());
	$('#priv2').html(this.p.toString());
	$('#pub-y-1').html(this.myPriv.toString());
	$('#test1').html(this.mod.toString());
	/********************************/
	
	function fastpow(a, n, m)
	{	
		sch = 0; 
		while(n > 1)
		{
			if (n.mod(2)=='0') {
				n=n.divide('2');
				a=a.multiply(a);
				a = a.mod(m);
				sch++;
			}
			else
			{
				n=n.minus(1);
				a=a.multiply(a);
				a = a.mod(m);
			}
		}
		return a;
	}
	
	function pick_rand() {
		var n = new BigInteger('20352011416290410787399145795734689202798264199');
		var n1 = n.subtract(BigInteger.ONE);
		var r = new BigInteger(n.bitLength(), rng);
		var integer = new bigInt(r.mod(n1).add(BigInteger.ONE).toString());
		return integer;
	}
}