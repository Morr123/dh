﻿<?php
include('vendor/autoload.php');

use PHP\Math\BigNumber\BigNumber;

class DH
{
	private $a;
	private $p;
	private $private_key;
	private $mod_key_klient;
	public $mod_key;
	public $secret_key;
	
	function __construct($a, $p)
	{
		$this->a = new BigNumber($a);
		$this->p = new BigNumber($p);
		$this->private_key = new BigNumber($this->BigRandomNumber('100000000000000000000000000000000000000000000', '99999999999999999999999999999999999999999999999'));
		$this->mod_key = $this->fastpow($this->a, $this->private_key, $this->p);
	}
	
	public function generateSecret($mod)
	{
		$mod = new BigNumber($mod);
		$this->secret_key = $this->fastpow($mod, $this->private_key, $this->p);
	}
	
	
	private function fastpow($a, $n, $m)
	{
		$n = new BigNumber($n->getValue());
		$a = new BigNumber($a->getValue());
		$m = new BigNumber($m->getValue());
		while($n != '1')
		{
			$mod = new BigNumber($n);
			if($mod->mod('2') != '0')
			{
				$n = $n->subtract(1);
				$a = $a->multiply($a->getValue());
				$a = $a->mod($m->getValue());
			}
			else 
			{
				$n->divide(2);
				$a->multiply($a);
				$a->mod($m);
			}
		}
		return $a;
	} 
	
	private function BigRandomNumber($min, $max) 
	{
		$difference   = bcadd(bcsub($max,$min),1);
		$rand_percent = bcdiv(mt_rand(), mt_getrandmax(), 47); // 0 - 1.0
		return bcadd($min, bcmul($difference, $rand_percent, 47), 0);
	}
}

if(isset($_POST['dh']))
{
		$a = $_POST['dh']['a'];
		$p = $_POST['dh']['p'];
		$mod = $_POST['dh']['mod'];
		
		$dh = new DH($a, $p);
		$dh->generateSecret($mod);
		
		$arr = [];
		$arr['mod'] = $dh->mod_key->getValue();
		
		file_put_contents('secret.txt', $dh->secret_key->getValue());
		$arr = json_encode($arr);
		echo $arr;
}
if(isset($_POST['data']))
{
	$data = $_POST['data'];
	$key = file_get_contents('secret.txt');
		
	$decrypted = cryptoJsAesDecrypt($key, $data);
	$request = $decrypted['data'];
		
	$response = 'Вы написали: ' . $request;
	$encrypted = cryptoJsAesEncrypt($key, $response);
		
	echo $encrypted;
}

function cryptoJsAesDecrypt($passphrase, $jsonString){
    $jsondata = json_decode($jsonString, true);
    $salt = hex2bin($jsondata["s"]);
    $ct = base64_decode($jsondata["ct"]);
    $iv  = hex2bin($jsondata["iv"]);
    $concatedPassphrase = $passphrase.$salt;
    $md5 = array();
    $md5[0] = md5($concatedPassphrase, true);
    $result = $md5[0];
    for ($i = 1; $i < 3; $i++) {
        $md5[$i] = md5($md5[$i - 1].$concatedPassphrase, true);
        $result .= $md5[$i];
    }
    $key = substr($result, 0, 32);
    $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
    return json_decode($data, true);
}

/**
* Encrypt value to a cryptojs compatiable json encoding string
*
* @param mixed $passphrase
* @param mixed $value
* @return string
*/
function cryptoJsAesEncrypt($passphrase, $value){
    $salt = openssl_random_pseudo_bytes(8);
    $salted = '';
    $dx = '';
    while (strlen($salted) < 48) {
        $dx = md5($dx.$passphrase.$salt, true);
        $salted .= $dx;
    }
    $key = substr($salted, 0, 32);
    $iv  = substr($salted, 32,16);
    $encrypted_data = openssl_encrypt(json_encode($value), 'aes-256-cbc', $key, true, $iv);
    $data = array("ct" => base64_encode($encrypted_data), "iv" => bin2hex($iv), "s" => bin2hex($salt));
    return json_encode($data);
}

?>